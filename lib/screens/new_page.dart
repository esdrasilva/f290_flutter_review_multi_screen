import 'package:flutter/material.dart';

class NewPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('New Page'),
      ),
      body: Column(
        children: [
          Text('Body Scaffold'),
          RaisedButton(onPressed: (){
            Navigator.pop(context);
          })
        ],        
      ),
    );
  }
}
